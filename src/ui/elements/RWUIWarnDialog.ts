import {
    Page,
    RevertContext,
    User,
    Warning,
    WarningOptions,
} from "app/mediawiki";
import { RWUIDialog, RWUIDialogProperties } from "app/ui/elements/RWUIDialog";

export interface RWUIWarnDialogProps extends RWUIDialogProperties {
    rollbackContext?: RevertContext;
    targetUser?: User;
    defaultWarnReason?: Warning;
    defaultWarnLevel?: number;
    relatedPage?: Page;
}

export class RWUIWarnDialog extends RWUIDialog<WarningOptions | null> {
    show(): Promise<WarningOptions> {
        throw new Error("Attempted to call abstract method");
    }
    render(): HTMLDialogElement {
        throw new Error("Attempted to call abstract method");
    }

    public static readonly elementName = "apparnDialog";

    constructor(readonly props: RWUIWarnDialogProps) {
        super(props);
    }
}
