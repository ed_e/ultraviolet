# redwarn-web (Development Branch)

RedWarn's userscript - made with TypeScript, Webpack, and TSX-DOM.

The [**R**ecent **Ed**its Patrol and **Warn**ing Tool](https://en.wikipedia.org/wiki/WP:RedWarn) (abbreviated as RedWarn) is a [Wikipedia](https://en.wikipedia.org/wiki/) [patrol](https://en.wikipedia.org/wiki/WP:RCP) and [counter-vandalism](https://en.wikipedia.org/wiki/WP:VD) tool, designed to be a user-friendly way to perform common moderation tasks.

You can help! If you find any bugs or would like new features, you can fix these or add them yourself. More technical documentation is coming in the user guide soon to help ease this process.

## Contributors

RedWarn is primarily maintained and developed by the RedWarn contributors.

- **[@ChlodAlejandro](https://gitlab.com/ChlodAlejandro)** - ([\[\[User:Chlod\]\]](https://en.wikipedia.org/wiki/User:Chlod)) - project lead, development
- **[@sportshead](https://gitlab.com/sportshead)** - ([\[\[User:Sportzpikachu\]\]](https://en.wikipedia.org/wiki/User:Sportzpikachu)) - assistant project lead, development
- **[@pr0mpted](https://gitlab.com/pr0mpted)** - ([\[\[User:Prompt0259\]\]](https://en.wikipedia.org/wiki/User:Prompt0259)) - development
- **[@leijurv](https://gitlab.com/leijurv)** - ([\[\[User:Leijurv\]\]](https://en.wikipedia.org/wiki/User:Leijurv)) - development and advice
- **[@Rexogamer](https://gitlab.com/Rexogamer)** - ([\[\[User:Remagoxer\]\]](https://en.wikipedia.org/wiki/User:Remagoxer)) - development
- **[@Asartea](https://gitlab.com/Asartea)** - ([\[\[User:Asartea\]\]](https://en.wikipedia.org/wiki/User:Asartea)) - documentation
- **[@Berrely](https://gitlab.com/Berrely)** - ([\[\[User:Berrely\]\]](https://en.wikipedia.org/wiki/User:Berrely)) - documentation
- **[@Sennecaster](https://gitlab.com/Sennecaster)** - ([\[\[User:Sennecaster\]\]](https://en.wikipedia.org/wiki/User:Sennecaster)) - documentation
- **[@ed_e](https://gitlab.com/ed_e)** ([\[\[User:Ed6767\]\]](https://en.wikipedia.org/wiki/User:Ed6767)) - original creator
- **[and everyone else on the RedWarn development team](https://en.wikipedia.org/wiki/Wikipedia:RedWarn/Team)**
- **[with some additional development from Wikipedia editors](https://en.wikipedia.org/wiki/WP:RW#Credits)**
- **[and other contributors.](https://gitlab.com/redwarn/redwarn-web/-/graphs/master)**

## Development

If you wish to contribute in the development of RedWarn, follow the instructions on our [development wiki](https://redwarn.toolforge.org/wiki/Development) file.

## License

Copyright © 2020–2021 The RedWarn Contributors. Released under the Apache License 2.0. Transpiled versions on Wikimedia Foundation wikis are sublicensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License, and GNU Free Documentation License 1.3. See [LICENSE](/LICENSE) for more information.
