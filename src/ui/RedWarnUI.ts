import StyleManager from "app/styles/StyleManager";
import { RWUIToast } from "./elements/RWUIToast";
import { RWUIDiffIcons } from "app/ui/elements/RWUIDiffIcons";
import { RWUIPageIcons } from "app/ui/elements/RWUIPageIcons";
import { RWUIAlertDialog } from "app/ui/elements/RWUIAlertDialog";
import { RWUIInputDialog } from "app/ui/elements/RWUIInputDialog";
import { RWUISelectionDialog } from "app/ui/elements/RWUISelectionDialog";
import { RWUIIFrameDialog } from "app/ui/elements/RWUIIFrameDialog";
import { RWUIWarnDialog } from "app/ui/elements/RWUIWarnDialog";
import { RWUIExtendedOptions } from "app/ui/elements/RWUIExtendedOptions";
import { RWUIProtectionRequestDialog } from "app/ui/elements/RWUIProtectionRequestDialog";
import { RWUIReportingDialog } from "app/ui/elements/RWUIReportingDialog";
import { RWUIPreferencesTab } from "app/ui/elements/RWUIPreferencesTab";
import { RWUIPreferencesItem } from "./elements/RWUIPreferencesItem";
import { RWUIPreferences } from "./elements/RWUIPreferences";

/**
 * Redirect class for easy access. UI elements of RedWarn are also created here.
 */
export default class RedWarnUI {
    /** Alias of {@link StyleManager.activeStyle.classMap.rwAlertDialog} */
    static get AlertDialog(): typeof RWUIAlertDialog {
        return StyleManager.activeStyle.classMap.rwAlertDialog;
    }
    /** Alias of {@link StyleManager.activeStyle.classMap.rwInputDialog} */
    static get InputDialog(): typeof RWUIInputDialog {
        return StyleManager.activeStyle.classMap.rwInputDialog;
    }
    /** Alias of {@link StyleManager.activeStyle.classMap.rwSelectionDialog} */
    static get SelectionDialog(): typeof RWUISelectionDialog {
        return StyleManager.activeStyle.classMap.rwSelectionDialog;
    }
    /** Alias of {@link StyleManager.activeStyle.classMap.apparnDialog} */
    static get WarnDialog(): typeof RWUIWarnDialog {
        return StyleManager.activeStyle.classMap.apparnDialog;
    }
    /** Alias of {@link StyleManager.activeStyle.classMap.rwProtectionRequestDialog} */
    static get ProtectionRequestDialog(): typeof RWUIProtectionRequestDialog {
        return StyleManager.activeStyle.classMap.rwProtectionRequestDialog;
    }
    /** Alias of {@link StyleManager.activeStyle.classMap.rwIFrameDialog} */
    static get IFrameDialog(): typeof RWUIIFrameDialog {
        return StyleManager.activeStyle.classMap.rwIFrameDialog;
    }
    /** Alias of {@link StyleManager.activeStyle.classMap.rwToast} */
    static get Toast(): typeof RWUIToast {
        return StyleManager.activeStyle.classMap.rwToast;
    }
    /** Alias of {@link StyleManager.activeStyle.classMap.rwDiffIcons} */
    static get DiffIcons(): typeof RWUIDiffIcons {
        return StyleManager.activeStyle.classMap.rwDiffIcons;
    }
    /** Alias of {@link StyleManager.activeStyle.classMap.rwPageIcons} */
    static get PageIcons(): typeof RWUIPageIcons {
        return StyleManager.activeStyle.classMap.rwPageIcons;
    }
    /** Alias of {@link StyleManager.activeStyle.classMap.rwExtendedOptions} */
    static get ExtendedOptions(): typeof RWUIExtendedOptions {
        return StyleManager.activeStyle.classMap.rwExtendedOptions;
    }
    /** Alias of {@link StyleManager.activeStyle.classMap.rwReportingDialog} */
    static get ReportingDialog(): typeof RWUIReportingDialog {
        return StyleManager.activeStyle.classMap.rwReportingDialog;
    }
    /** Alias of {@link StyleManager.activeStyle.classMap.rwPreferences} */
    static get Preferences(): typeof RWUIPreferences {
        return StyleManager.activeStyle.classMap.rwPreferences;
    }
    /** Alias of {@link StyleManager.activeStyle.classMap.rwPreferencesTab} */
    static get PreferencesTab(): typeof RWUIPreferencesTab {
        return StyleManager.activeStyle.classMap.rwPreferencesTab;
    }
    /** Alias of {@link StyleManager.activeStyle.classMap.rwPreferencesItem} */
    static get PreferencesItem(): typeof RWUIPreferencesItem {
        return StyleManager.activeStyle.classMap.rwPreferencesItem;
    }
}

/**
 * A complete list of all RWUIElements
 */
export const RWUIElements = {
    [RWUIAlertDialog.elementName]: RWUIAlertDialog,
    [RWUIInputDialog.elementName]: RWUIInputDialog,
    [RWUISelectionDialog.elementName]: RWUISelectionDialog,
    [RWUIWarnDialog.elementName]: RWUIWarnDialog,
    [RWUIProtectionRequestDialog.elementName]: RWUIProtectionRequestDialog,
    [RWUIIFrameDialog.elementName]: RWUIIFrameDialog,
    [RWUIToast.elementName]: RWUIToast,
    [RWUIDiffIcons.elementName]: RWUIDiffIcons,
    [RWUIPageIcons.elementName]: RWUIPageIcons,
    [RWUIExtendedOptions.elementName]: RWUIExtendedOptions,
    [RWUIReportingDialog.elementName]: RWUIReportingDialog,
    [RWUIPreferences.elementName]: RWUIPreferences,
    [RWUIPreferencesTab.elementName]: RWUIPreferencesTab,
    [RWUIPreferencesItem.elementName]: RWUIPreferencesItem,
};
