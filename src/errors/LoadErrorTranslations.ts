// Translations should be provided for all Wikipedia wikis with 1,000,000+ articles
// and wikis which have RedWarn support.
export default <Record<string, string[]>>{
    ar: [
        // Right-to-left language. Encode in Base64 to avoid bidirectional IDE editing.
        // Machine-translated.
        atob("2YHYtNmEINiq2K3ZhdmK2YQgUmVkV2Fybi4="),
        atob(
            "2KXYsNinINin2LPYqtmF2LHYqiDYp9mE2YXYtNmD2YTYqSDYjCDZgdmK2LHYrNm" +
                "JINin2YTYp9iq2LXYp9mEINio2YXYt9mI2LHZiiBSZWRXYXJuLiDZitiq2YUg2K" +
                "rZiNmB2YrYsSDZhdi52YTZiNmF2KfYqiDYqti12K3ZititINil2LbYp9mB2YrYq" +
                "SDYo9iv2YbYp9mHLg=="
        ),
        atob(
            "2KXYsNinINi32YTYqCDYo9it2K8g2KPYudi22KfYoSDZgdix2YrZgiBSZWRXYXJ" +
                "uINiMINmK2LHYrNmJINiq2YLYr9mK2YUg2KfZhNmG2LUg2YXZhiDYrtmE2KfZhC" +
                "DZgtmG2KfYqSDYrtin2LXYqSAo2KjYsdmK2K8g2KXZhNmD2KrYsdmI2YbZiiDYj" +
                "CBJUkMg2Iwg2KXZhNiuKS4="
        ),
        atob(
            "2YrZhdmD2YbZgyDYp9mE2YbZgtixINio2LLYsSDYp9mE2YXYp9mI2LMg2KfZhNi" +
                "j2YrZhdmGINmB2YjZgiDYp9mE2YbYtSDZiNiq2K3Yr9mK2K8gItmG2LPYriDZg9" +
                "in2KbZhiIg2YTZhNmG2LPYriDYqNi02YPZhCDYo9iz2LHYuS4="
        ),
    ],
    de: [
        // Machine-translated.
        "RedWarn konnte nicht geladen werden.",
        "Wenn das Problem weiterhin besteht, wenden Sie sich bitte an die RedWarn-Entwickler. " +
            "Zusätzliche Informationen zur Fehlersuche finden Sie unten.",
        "Wenn Sie von einem RedWarn-Teammitglied darum gebeten werden, " +
            "übermitteln Sie den Text bitte über einen privaten Kanal (E-Mail, IRC, etc.).",
        "Sie können mit der rechten Maustaste auf den Text klicken und " +
            '"Objekt kopieren" wählen, um ihn schneller zu kopieren.',
    ],
    en: [
        "Ultraviolet failed to load.",
        "If the problem persists, please contact Ultraviolet's developers. Additional debug information is provided below.",
        "If requested by an Ultraviolet team member, please provide the text through a private channel (email, IRC, etc.).",
        'You can right click the text and select "Copy Object" to copy faster.',
    ],
    es: [
        // Machine-translated.
        "No se ha podido cargar RedWarn.",
        "Si el problema persiste, póngase en contacto con los desarrolladores de RedWarn. " +
            "A continuación se proporciona información adicional de depuración.",
        "Si lo solicita un miembro del equipo de RedWarn, por favor proporcione el texto " +
            "a través de un canal privado (correo electrónico, IRC, etc.).",
        "Puede hacer clic con el botón derecho del ratón en el texto y seleccionar " +
            '"Copiar objeto" para copiarlo más rápidamente.',
    ],
    fr: [
        // Machine-translated.
        "Le chargement de RedWarn a échoué.",
        "Si le problème persiste, veuillez contacter les développeurs de RedWarn. " +
            "Des informations de débogage supplémentaires sont fournies ci-dessous.",
        "Si un membre de l'équipe RedWarn vous le demande, veuillez fournir le texte " +
            "par un canal privé (email, IRC, etc.).",
        "Vous pouvez faire un clic droit sur le texte et sélectionner " +
            '"Copier l\'objet" pour copier plus rapidement.',
    ],
    nl: [
        // Machine-translated.
        "RedWarn is niet geladen.",
        "Als het probleem aanhoudt, neem dan contact op met de ontwikkelaars van RedWarn. " +
            "Aanvullende debug-informatie wordt hieronder verstrekt.",
        "Indien een RedWarn teamlid hierom vraagt, kunt u de tekst via een privé kanaal " +
            +"(email, IRC, etc.) aanleveren.",
        'U kunt rechts op de tekst klikken en "Copy Object" selecteren om sneller te kopiëren.',
    ],
    ja: [
        "RedWarnのロードに失敗しました。",
        "問題は解決しない場合は、RedWarnの開発者にお問い合わせください。",
        "RedWarnの開発者から要請があった場合は、プライベートな方法（メールとかIRCなど）通じてテキストを提供してください。",
        "テキストを右クリックして「オブジェクトのコピー」を選択すると、より速くコピーはできます。",
    ],
    zh: [
        // Machine-translated.
        "RedWarn 加载失败。",
        "如果问题仍然存在，请联系RedWarn的开发者。下面提供了额外的调试信息。",
        "如果RedWarn团队成员要求，请通过私人渠道（电子邮件、IRC等）提供文本。",
        '你可以右击文本并选择 "复制对象" 来更快地复制。',
    ],
};
