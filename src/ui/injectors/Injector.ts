export interface Injector {
    init(): Promise<void>;
}
