export default function (target: string): string {
    return target.charAt(0).toUpperCase() + target.slice(1);
}
