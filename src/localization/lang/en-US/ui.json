{
    "unfinished": "Hmm, looks like we haven't implemented this feature yet. Check back later!",
    "ok": "OK",
    "cancel": "Cancel",
    "confirm": "Confirm",
    "undo": "Undo",
    "redo": "Redo",
    "toggleSwitch": "Off/On",
    "toggleCheckbox": "Off/On",
    "extendedOptions": {
        "title": "More Options",
        "extraRevertOptions": "Additional revert options"
    },
    "pageIcons": {
        "message": "New Message",
        "quickTemplate": "Quick Template",
        "warn": "Warn User",
        "protection": "Page Protection",
        "alertOnChange": "Alert on Change",
        "latestRevision": "Latest Revision",
        "moreOptions": "More Options",
        "vandalismStatistics": "Vandalism statistics",
        "report": "Report to {{name}}",
        "preferences": "Preferences",
        "uvTalk": "Ultraviolet's talk page"
    },
    "restore": {
        "label": "Restore reason",
        "helperText": "Enter a reason for this restore",
        "actions": {
            "ok": "Restore"
        }
    },
    "rollback": {
        "label": "Rollback reason",
        "helperText": "Enter a reason for this rollback",
        "actions": {
            "ok": "Rollback"
        }
    },
    "copyURL": {
        "button": "Copy URL",
        "success": "URL copied.",
        "failure": "Failed to copy URL. Have you given the browser proper permissions?"
    },
    "toasts": {
        "undone": "Action undone.",
        "viewAction": "View",
        "pleaseWait": "Please wait...",
        "restoreError": "Sorry, there was an error, likely an edit conflict. This edit was not restored.",
        "rollbackError": "Sorry, there was an error performing a rollback.",
        "newerRev": "A newer revision was detected. Your revert was not applied.",
        "userUndefined": "Sorry, the target user does not exist, cannot be found, or is hidden from the public. Your action was not completed.",
        "userWarned": "User warned.",
        "userWarnedAction": "View",
        "userWarnFailed": "There was a problem warning the user. Please verify if the user has been warned before issuing another warning.",
        "protectionRequested": "Protection requested.",
        "protectionRequestFailed": "There was a problem requesting protection. Please verify if the request has been made before requesting again."
    },
    "watch": {
        "denied": "Permission to show notifications denied.",
        "watching": "Watching for changes on {{page}}. Keep this tab open.",
        "stoppedWatching": "Stopped watching for changes on {{page}}.",
        "prefix": "[New edit(s)!] ",
        "notification": {
            "title": "New edit to {{page}}",
            "title_plural": "{{count}} new edits to {{page}}",
            "header": "Click here to view.",
            "diff": "Latest revision by {{author}} ({{since}}): \"{{comment}}\".",
            "diff_nosummary": "Latest revision by {{author}} ({{since}})."
        }
    },
    "rollbackAvailableDialog": {
        "actions": {
            "rollback": "Use rollback",
            "revert": "Keep using rollback-like"
        },
        "content": "You have rollback permissions! Would you like to use the faster rollback API in future or continue using a rollback-like setting? You can change this in your preferences at any time."
    },
    "tamperProtection": {
        "header": "Attention!",
        "warningContent": "You are currently using RedWarn in a way that is strongly discouraged under guidelines set at <a href=\"https://en.wikipedia.org/wiki/Wikipedia:RW/A\" title=\"Wikipedia:RW/A\">Wikipedia:RedWarn/Abuse</a>. This will be permanently recorded and will prevent you from running legitimate installations of RedWarn. Please contact a RedWarn team developer for assistance."
    },
    "userSelect": {
        "change": "Change the target user",
        "confirm": "Target this user",
        "load_wait": "Please be patient while the user information is being loaded.",
        "loading": "Loading user information...",
        "input": "Target User",
        "highestLevel": "Highest warning level",
        "levelInfo": "The user's highest warning level",
        "levelInfo_none": "User has not received any warnings this month",
        "levelInfo_notice": "User has received a level 1 notice this month",
        "levelInfo_caution": "User has received a level 2 notice this month",
        "levelInfo_warning": "User has received a level 3 warning this month. $t(ui:userSelect.report)",
        "levelInfo_final": "User has received a level 4 final warning this month. $t(ui:userSelect.report)",
        "levelInfo_immediate": "User has received an immediate level 4 final warning this month. $t(ui:userSelect.report)",
        "report": "Click to report.",
        "edits": "{{edits}} edits",
        "age": "Joined {{localeAge}}",
        "talk": {
            "main": "User talk page",
            "month": "Notices for this month",
            "whole": "Entire talk page"
        },
        "show": {
            "userpage": "Show user page",
            "contributions": "Show user contributions"
        },
        "missing": "User does not exist.",
        "invalid": "The username provided is not valid.",
        "fail": "Failed to load user information. Try again in a bit."
    },
    "validation": {
        "dialog": {
            "title": "Issue found",
            "title_plural": "Issues found",
            "intro": "The following issues are preventing you from continuing:"
        },
        "tooltip": {
            "fail": "There are issues which are preventing you from continuing. Click here for more information.",
            "pass": "No issues were found."
        }
    },
    "warn": {
        "title": "Warn User",
        "reason": {
            "page": "Related page",
            "recentPageOpenerTooltip": "Recent pages",
            "additionalText": "Additional text",
            "levelSelectionLevelNotPresent": "There is no level {{level}} ({{levelReadable}}) template available.",
            "levelSelectionLevel": "Level {{level}} {{levelReadable}}: {{levelDescription}}",
            "searchDialogOpenerTooltip": "Search for a warning",
            "warningSelectionDropdownTitle": "Warning",
            "singleIssueTemplate": "Reminder",
            "policyViolationTemplate": "Policy Violation Warning",
            "warningLevel": "Warning level",
            "noWarningSelected": "No warning selected."
        },
        "templateSearchDialog": {
            "dialogTitle": "Warnings",
            "searchBoxLabel": "Search for a warning...",
            "instantSelect": "Press ENTER to use this template",
            "tip": "Tip: Double-click a warning to quickly select it, or press ENTER to select the top result."
        },
        "recentPagesDialog": {
            "dialogTitle": "Recent Pages"
        },
        "validation": {
            "fail": "An unknown issue ocurred.",
            "fail_template": "You don't have a warning template selected.",
            "fail_level": "You haven't selected a warning level.",
            "fail_user": "You haven't selected a target user.",
            "fail_self": "You are not allowed to warn yourself.",
            "fail_required": "Some required fields have not been filled in.",
            "failDetailed": "$t(ui:warn.validation.fail) If this problem persists, inform the developers.",
            "failDetailed_template": "$t(ui:warn.validation.fail_template) Select one from the dropdown or search tool.",
            "failDetailed_level": "$t(ui:warn.validation.fail_level) Please select an appropriate level.",
            "failDetailed_user": "$t(ui:warn.validation.fail_user) Please enter a username in the target user box.",
            "failDetailed_self": "$t(ui:warn.validation.fail_self) If you want to test, please use a sandbox user.",
            "failDetailed_required": "Some required fields have not been filled in. Please fill in the required text fields (denoted by an asterisk) found below the warning to be issued.",
            "pass": "No issues were found."
        },
        "risky": {
            "title": "Risky warning",
            "content": "You are about to warn {{group, en-handle-an}} {{group}}. Are you sure about this?"
        },
        "ok": "Warn User"
    },
    "diff": {
        "progress": {
            "prepare": "Preparing...",
            "details": "Getting revision information...",
            "revert": "Reverting...",
            "revert_rollback": "Rolling back...",
            "restore": "Restoring revision...",
            "finish": "Revert completed.",
            "finish_nochange": "Revert requested but no changes were made. A revert must have already been applied."
        }
    },
    "close": "Close",
    "configErrorDialog": {
        "title": "Configuration Error",
        "content": "Your RedWarn configuration could not be loaded correctly and has been reset. If this issue persists, please contact the RedWarn team.",
        "actions": [
            {
                "data": "$t(ui:ok)"
            }
        ]
    },
    "styleError": {
        "missing": {
            "title": "Style Missing",
            "content": "The style set in your RedWarn configuration cannot be found. It has been set to the default style for now.",
            "actions": [
                {
                    "data": "$t(ui:ok)"
                }
            ]
        }
    },
    "contribs": {
        "previewLink": "prev",
        "previewTooltip": "Preview Rollback",
        "vandalLink": "rvv",
        "vandalTooltip": "Quick Rollback Vandalism",
        "rollbackLink": "rb",
        "rollbackTooltip": "Rollback"
    },
    "protectionRequest": {
        "title": "Request Page Protection",
        "ok": "Request",
        "page": {
            "label": "Target page",
            "change": "Change the target page",
            "loadingText": "Loading protection information...",
            "inputSubmit": "Target this page",
            "inputCancel": "Cancel"
        },
        "info": {
            "unprotected": "Page is not protected.",
            "cascaded": "cascaded",
            "cascading": "cascading",
            "note": " ({{detail}})",
            "detailed": "{{statusName}} until {{date}}{{note}}",
            "detailed_indefinite": "Indefinitely {{statusName}}{{note}}",
            "fallback": "{{type}} protected against non-{{level}} users until {{date}}{{note}}",
            "fallback_indefinite": "Indefinitely {{type}} against for non-{{level}} users{{note}}"
        },
        "reasons": {
            "label": "Reason",
            "other": "Other reason"
        },
        "additionalInformation": "Additional information",
        "duration": {
            "label": "Duration",
            "temporary": "Temporary",
            "indefinite": "Indefinite"
        },
        "notice": "You should not consider requesting page protection as a method for continuing an argument from elsewhere nor as a venue for starting a new discussion regarding article content. If a request contains excessive argument, appears to be intended to resolve a content dispute, includes personal attacks or uncivil comments, or has any other unrelated discussion, it will be removed and no action will be taken.",
        "validation": {
            "fail": "An unknown issue ocurred.",
            "fail_noLevel": "You don't have a protection level selected.",
            "fail_noDuration": "You don't have a protection duration selected.",
            "fail_levelEqual": "The level and duration requested is already applied.",
            "fail_noReason": "No reason was provided.",
            "fail_noAdditionalReason": "No additional reason was provided.",
            "failDetailed": "$t(ui:protectionRequest.validation.fail) If this problem persists, inform the developers.",
            "failDetailed_noLevel": "$t(ui:protectionRequest.validation.fail_noLevel) Select one from the left-side panel.",
            "failDetailed_noDuration": "$t(ui:protectionRequest.validation.fail_noDuration) Select either \"Temporary\" or \"Indefinite\" from the provided options.",
            "failDetailed_levelEqual": "$t(ui:protectionRequest.validation.fail_levelEqual) Please select a protection level or duration different from the current protection level/duration.",
            "failDetailed_noReason": "$t(ui:protectionRequest.validation.fail_noReason) A reason must be provided for protection requests.",
            "failDetailed_noAdditionalReason": "$t(ui:protectionRequest.validation.fail_noAdditionalReason) Please provide an additional reason, or switch to a provided reason using the dropdown."
        },
        "retarget": {
            "text": "Ultraviolet could not determine if this is an increase or decrease in the page's protection level. This is likely due to a problem in Ultraviolet's per-wiki configuration - please indicate whether this request is an increase or decrease in the page's protection level.",
            "increase": "Increase",
            "decrease": "Decrease"
        },
        "done": "Request filed."
    },
    "reporting": {
        "title": "Report to {{venue}}",
        "ok": "Report",
        "target": {
            "label": "Target",
            "label_user": "User",
            "label_page": "Page",
            "helperText": "Enter a page",
            "helperText_user": "Enter a username",
            "nonexistent_page": "The page you entered does not exist."
        },
        "info": {
            "reason": {
                "label": "Reason",
                "other": "Other reason",
                "default": "Default reasons"
            },
            "comments": {
                "label": "Additional information",
                "placeholder": "Provide additional details about your request."
            }
        },
        "validation": {
            "fail": "An unknown issue ocurred.",
            "fail_target": "There is no target to be reported.",
            "fail_targetMissing": "The page to be reported does not exist.",
            "fail_self": "You are not allowed to report yourself.",
            "fail_reason": "You did not provide a reason for the report.",
            "fail_short": "Your report is believed to be too short to contain substantial information.",
            "fail_exists": "The target has already been reported.",
            "failDetailed": "An unknown issue ocurred. Please try again.",
            "failDetailed_target": "There is no target to be reported. Please make sure that you've selected a valid page or user.",
            "failDetailed_targetMissing": "The page to be reported does not exist. Please make sure that the page you are attempting to report exists.",
            "failDetailed_self": "You are not allowed to report yourself, even for testing situations.",
            "failDetailed_reason": "You did not provide a reason for the report. Please choose a prefilled reason or write your own.",
            "failDetailed_short": "Your report is believed to be too short to contain substantial information. Please make sure that your report contains enough detailed information to be useful.",
            "failDetailed_exists": "The target has already been reported. You do not need to file this report anymore."
        },
        "restricted": {
            "title": "Risky report",
            "text": "You are about to report {{group, en-handle-an}} {{group}}. This may be the incorrect venue for such a report. Are you sure about this?"
        },
        "done": "Report filed.",
        "done_email": "Report emailed."
    },
    "newMessage": {
        "label": "New Message",
        "doing": "Sending message...",
        "done": "Message sent.",
        "doneAction": "View",
        "fail": "Failed to send the message."
    }
}